# Setup Fedora for classes on Department of mathematics and informatics

First clone this repository

```sh
git clone https://gitlab.com/dmiuns/lid/fedora-base.git
```

Then install ansible and required plugins
```sh
sudo dnf install ansible
ansible-galaxy collection install community.general
```

After that `cd` into the cloned repo and run the Ansible playbook
```sh
ansible-playbook -i localhost, -c local -t <tags> -K main.yml
```

The tags you can specify are the following (use comma separation)

|Tag    |Description                             |
|-------|----------------------------------------|
|base   |The base system                         |
|update |Update the system (this can be skipped) |
|devel  |Setup development environment           |
|drivers|Install various drivers (H.264)         |
|nvidia |Install proprietary Nvidia drivers      |
|extra  |Install extra packges (Chrome, MS Teams)|
|network|Setup PMF networks                      |
|gnome  |Configure Gnome to be more like Windows |

You will be prompted for the `BECOME password` which is the password for the superuser. It's needed to run tasks that require superuser (eg. dnf).

You will also be prompted for `PMF username`, `PMF password` and `Wireless interface` which are needed for network configuration.
The username is you faculty issued email adress and the password is the password said email. Wireless interface is required if you're on a machine that has WiFi so access to the wireless network on the faculty (PMF) and campus (eduroam) will be configured. You can check what is the name of the interface by running command `ip a`. Please check with support staff to make sure what is the correct name of the interface.

After everything is complete, you may remove the installed ansible packages
```sh
sudo dnf remove anisble
```
